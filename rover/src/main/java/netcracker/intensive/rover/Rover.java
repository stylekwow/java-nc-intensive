package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.Direction;

import java.util.HashMap;

public class Rover implements Landable, Liftable, Moveable, Turnable {
    private Point position;
    private Direction direction;
    private GroundVisor visor;
    private boolean flight;
    private HashMap<Integer, String> stats;

    {
        position = new Point(0, 0);
        direction = Direction.SOUTH;
        flight = false;
    }

    public Rover(GroundVisor groundVisor) {
        this.visor = groundVisor;
    }

    public Rover(GroundVisor groundVisor, HashMap<Integer, String> stats) {
        this.stats = stats;
        this.visor = groundVisor;
    }

    public Point getCurrentPosition() {
        return position;
    }

    public Direction getDirection() {
        return direction;
    }

    public boolean isAirborne() {
        return flight;
    }

    public void moveSouth() throws OutOfGroundException {
        if (!visor.hasObstacles(new Point(position.getX(), position.getY() + 1))) {
            position.setY(position.getY() + 1);
        }
    }

    public void moveNorth() throws OutOfGroundException {
        if (!visor.hasObstacles(new Point(position.getX(), position.getY() - 1))) {
            position.setY(position.getY() - 1);
        }
    }

    public void moveEast() throws OutOfGroundException {
        if (!visor.hasObstacles(new Point(position.getX() + 1, position.getY()))) {
            position.setX(position.getX() + 1);
        }
    }

    public void moveWest() throws OutOfGroundException {
        if (!visor.hasObstacles(new Point(position.getX() - 1, position.getY()))) {
            position.setX(position.getX() - 1);
        }
    }

    @Override
    public void land(Point position, Direction direction) {
        try {
            if (!visor.hasObstacles(position)) {
                this.position = position;
                this.direction = direction;
                flight = false;
            }
        } catch (OutOfGroundException e) {
            lift();
        }
    }

    @Override
    public void lift() {
        if (!flight) {
            flight = true;
        }
        direction = null;
        position = null;
    }

    @Override
    public void move() {
        if (position == null || direction == null) {
            return;
        }
        try {
            switch (direction) {
                case SOUTH:
                    moveSouth();
                    break;
                case NORTH:
                    moveNorth();
                    break;
                case WEST:
                    moveWest();
                    break;
                case EAST:
                    moveEast();
                    break;
            }
        } catch (OutOfGroundException e) {
            lift();
        }
    }

    @Override
    public void turnTo(Direction direction) {
        this.direction = direction;
    }
}
