package netcracker.intensive.rover;

import netcracker.intensive.rover.constants.CellState;

public class GroundCell {
    private CellState state;
    private Point point;

    public GroundCell(CellState state) {
        this.state = state;
    }

    public CellState getState() {
        return state;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
