package netcracker.intensive.rover;

public class Ground {
    private GroundCell[][] landscape;
    private int length;
    private int width;

    public void initialize(GroundCell ... groundCell) {
        int k = 0;
        if (length * width > groundCell.length) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < landscape.length; i++) {
            for (int j = 0; j < landscape[i].length; j++) {
                landscape[i][j] = groundCell[k++];
            }
        }
    }

    public Ground(int length, int width) {
        this.length = length;
        this.width = width;
        landscape = new GroundCell[length][width];
    }

    public GroundCell getCell(int x, int y) throws OutOfGroundException {
        if ((x < 0) || (x > length) || (y < 0) || (y > width)) {
            throw new OutOfGroundException();
        }
        return landscape[y][x];
    }
}
