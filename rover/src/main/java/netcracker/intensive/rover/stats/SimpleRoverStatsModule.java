package netcracker.intensive.rover.stats;

import netcracker.intensive.rover.Point;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SimpleRoverStatsModule implements RoverStatsModule {
    private List<Point> visited = new ArrayList<>();

    @Override
    public Collection<Point> getVisitedPoints() {
        return visited;
    }

    @Override
    public boolean isVisited(Point point) {
        for (Point p : visited) {
            if (point.equals(p)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void registerPosition(Point position) {
        for (Point p : visited) {
            if (position.equals(p)) {
                return;
            }
        }
        visited.add(position);
    }
}
