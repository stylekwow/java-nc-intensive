package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Landable;
import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.constants.Direction;
import netcracker.intensive.rover.programmable.ProgrammableRover;

public class LandCommand implements RoverCommand {
    private Rover rover;
    private Point point;
    private Direction direction;

    public LandCommand(ProgrammableRover rover, Point point, Direction direction) {
        this.direction = direction;
        this.point = point;
        this.rover = rover;
    }

    public LandCommand(Rover rover, Point point, Direction direction) {
        this.direction = direction;
        this.point = point;
        this.rover = rover;
    }

    @Override
    public void execute() {
        if (rover != null) {
            rover.land(point, direction);
        }
    }

    @Override
    public String toString() {
        return "Land at (" + point.getX() + ", " + point.getY() +") heading " + direction.toString();
    }

    @Override
    public boolean equals(Object ob){
        return ob.getClass().getName().equals("netcracker.intensive.rover.command.LandCommand");
    }
}
