package netcracker.intensive.rover.command;

import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.programmable.ProgrammableRover;

public class MoveCommand implements RoverCommand {
    private Rover rover;

    public MoveCommand(ProgrammableRover rover) {
        this.rover = rover;
    }

    public MoveCommand(Rover rover) {
        this.rover = rover;
    }

    @Override
    public void execute() {
        if (rover != null) {
            rover.move();
        }
    }

    @Override
    public String toString() {
        return "Rover moved";
    }

    @Override
    public boolean equals(Object ob) {
        return ob.getClass().getName().equals("netcracker.intensive.rover.command.MoveCommand");
    }

}
