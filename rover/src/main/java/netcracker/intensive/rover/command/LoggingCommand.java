package netcracker.intensive.rover.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingCommand implements RoverCommand {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingCommand.class);
    private MoveCommand move;

    public LoggingCommand(MoveCommand move) {
        this.move = move;
    }

    @Override
    public void execute() {
        LOGGER.debug("move");
        move.execute();
    }

    @Override
    public String toString() {
        return "Rover moved";
    }

    @Override
    public boolean equals(Object ob) {
        return ob.getClass().getName().equals("netcracker.intensive.rover.command.LoggingCommand");
    }
}
