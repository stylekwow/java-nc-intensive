package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.GroundVisor;
import netcracker.intensive.rover.Rover;
import netcracker.intensive.rover.command.LandCommand;
import netcracker.intensive.rover.command.LiftCommand;
import netcracker.intensive.rover.command.MoveCommand;
import netcracker.intensive.rover.command.RoverCommand;
import netcracker.intensive.rover.stats.SimpleRoverStatsModule;

import java.util.*;

/**
 * Этот класс должен уметь все то, что умеет обычный Rover, но при этом он еще должен уметь выполнять программы,
 * содержащиеся в файлах
 */
public class ProgrammableRover extends Rover implements ProgramFileAware {
    private SimpleRoverStatsModule statsModule;
    private Map<String, Object> settings;

    public ProgrammableRover(GroundVisor groundVisor, SimpleRoverStatsModule statsModule) {
        super(groundVisor);
        this.statsModule = statsModule;
    }

    @Override
    public void executeProgramFile(String path) {
        RoverCommandParser parser = new RoverCommandParser(this, path);
        RoverProgram program = parser.getProgram();
        settings = program.getSettings();
        List<RoverCommand> commands = program.getCommands();
        for (RoverCommand cmd : commands) {
            if (parser.isStatting()) {
                if (cmd instanceof MoveCommand) {
                    cmd.execute();
                    statsModule.registerPosition(this.getCurrentPosition());
                }
                else if (cmd instanceof LandCommand) {
                    cmd.execute();
                    statsModule.registerPosition(this.getCurrentPosition());
                }
                else {
                    cmd.execute();
                }

            }
            else {
                cmd.execute();
            }
        }
    }

    public Map<String, Object> getSettings() {
        return Collections.unmodifiableMap(settings);
    }

}
