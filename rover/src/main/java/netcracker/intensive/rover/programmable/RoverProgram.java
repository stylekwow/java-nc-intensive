package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.command.RoverCommand;

import java.util.*;

public class RoverProgram {
    public static final String LOG = "log";
    public static final String STATS = "stats";
    public static final String SEPARATOR = "===";
    private Map<String, Object> settings;
    private List<RoverCommand> commands;

    public Map<String, Object> getSettings() {
        return settings;
    }

    public List<RoverCommand> getCommands() {
        return commands;
    }

    public void setSettings (Map<String, Object> settings) {
        this.settings = settings;
    }

    public void setCommands(List<RoverCommand> commands) {
        this.commands = commands;
    }
}
