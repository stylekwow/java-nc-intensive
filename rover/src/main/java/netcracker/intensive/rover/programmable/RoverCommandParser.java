package netcracker.intensive.rover.programmable;

import netcracker.intensive.rover.Point;
import netcracker.intensive.rover.command.*;
import netcracker.intensive.rover.constants.Direction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoverCommandParser {
    private String filePath;
    private ProgrammableRover rover;
    private Map<String, Object> settings = new HashMap<>();
    private RoverProgram program = new RoverProgram();
    private List<RoverCommand> commands = new ArrayList<>();
    private boolean statting = false;

    public RoverCommandParser(ProgrammableRover rover, String filePath) {
        this.rover = rover;
        this.filePath = "C:\\Users\\Aleksey\\Documents\\javacode\\java-nc-intensive\\rover\\src\\test\\resources\\netcracker\\intensive\\rover\\programmable\\" + filePath;
        program.setCommands(commands);
        program.setSettings(settings);
        parseFile();
    }

    private void parseFile() {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String str;
            boolean logging = false;
            if (((str = br.readLine()) != null) && (str.startsWith(RoverProgram.LOG))) {
                if (str.endsWith("on")) {
                    settings.put(RoverProgram.LOG, true);
                    logging = true;
                }
                else {
                    settings.put(RoverProgram.LOG, false);
                }
            }
            if (((str = br.readLine()) != null) && (str.startsWith(RoverProgram.STATS))) {
                if (str.endsWith("on")) {
                    settings.put(RoverProgram.STATS, true);
                    statting = true;
                }
                else {
                    settings.put(RoverProgram.STATS, false);
                }
            }
            while (((str = br.readLine()) != null) && (!str.equals(""))) {
                if (str.equals("===")) {
                    continue;
                }
                int indexEndString = (str.indexOf(" ") == -1) ? str.length() : str.indexOf(" ");
                String tmp = str.substring(0, indexEndString);
                switch (tmp) {
                    case "move":
                        MoveCommand move = new MoveCommand(rover);
                        if (logging) {
                            commands.add(new LoggingCommand(move));
                        }
                        commands.add(move);
                        break;
                    case "turn":
                        Direction direction = null;
                        String direct = str.substring(indexEndString + 1, str.length());
                        direct = direct.toUpperCase();
                        switch (direct) {
                            case "SOUTH":
                                direction = Direction.SOUTH;
                                break;
                            case "NORTH":
                                direction = Direction.NORTH;
                                break;
                            case "WEST":
                                direction = Direction.WEST;
                                break;
                            case "EAST":
                                direction = Direction.EAST;
                                break;
                        }
                        commands.add(new TurnCommand(rover, direction));
                        break;
                    case "lift":
                        commands.add(new LiftCommand(rover));
                        break;
                    case "land":
                        String[] arrStr = str.split(" ");
                        Point point = new Point(Integer.parseInt(arrStr[1]), Integer.parseInt(arrStr[2]));
                        Direction direction1 = null;
                        String landDirect = arrStr[3].toUpperCase();
                        switch (landDirect) {
                            case "SOUTH":
                                direction1 = Direction.SOUTH;
                                break;
                            case "NORTH":
                                direction1 = Direction.NORTH;
                                break;
                            case "EAST":
                                direction1 = Direction.EAST;
                                break;
                            case "WEST":
                                direction1 = Direction.WEST;
                                break;
                        }
                        commands.add(new LandCommand(rover, point, direction1));
                        break;

                    }
                }
            } catch (Exception e) {
            throw new RoverCommandParserException(e);
        }
    }

    public RoverProgram getProgram() {
        return program;
    }

    public boolean isStatting() {
        return statting;
    }
}
